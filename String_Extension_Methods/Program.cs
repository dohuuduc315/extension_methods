﻿var word = "hello,    wolrd   !   ";
Console.WriteLine(word.WordCount());
Console.WriteLine(word.RemoveWhitespace());
Console.WriteLine(word.UppercaseFirstLetter());


static class StringExtensions
{
    public static int WordCount(this string str)
    {
        string[] words = str.Split(" ");
        return words.Length;
    }
    public static string RemoveWhitespace(this string str)
    {
        return new string(str.ToCharArray()
            .Where(x => !char.IsWhiteSpace(x))
            .ToArray());
    }
    public static string UppercaseFirstLetter(this string str)
    {
        if (string.IsNullOrEmpty(str))
        {
            return string.Empty;
        }
        char[] arr = str.ToCharArray();
        arr[0] = char.ToUpper(arr[0]);
        return new string(arr);
    }
}