﻿Student[] students = new Student[]
{
    new Student("Duc", 21, 7.5),
    new Student("Kiet", 21, 8),
    new Student("Dang", 21, 1),
    new Student("Hung", 21, 8.5),
    new Student("Vu", 21, 5.5),
};
foreach (var item in students)
{
    Console.WriteLine("{0}: Letter Grade = {1}, Is Passing = {2}", item.Name, item.GetLetterGrade(), item.IsPassing());
}

class Student
{
    public string Name { get; set; }
    public int Age { get; set; }
    public double GPA { get; set; }
    public Student(string name, int age, double gpa)
    {
        Name = name;
        Age = age;
        GPA = gpa;
    }
}
static class StudentExtensions
{
    public enum LetterGrade
    {
        A,
        B,
        C,
        D,
        F
    }
    public static LetterGrade GetLetterGrade(this Student student)
    {
        if (student.GPA >= 4.0 && student.GPA <= 5.0)
        {
            return LetterGrade.A;
        }
        else if (student.GPA >= 3.0 && student.GPA < 4.0)
        {
            return LetterGrade.B;
        }
        else if (student.GPA >= 2.0 && student.GPA < 3.0)
        {
            return LetterGrade.C;
        }
        else if (student.GPA >= 1.0 && student.GPA < 2.0)
        {
            return LetterGrade.D;
        }
        else
        {
            return LetterGrade.F;
        }
    }
    public static bool IsPassing(this Student student)
    {
        return student.GPA > 2.0;
    }
}